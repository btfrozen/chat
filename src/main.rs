extern crate time;
extern crate sync;
extern crate iron;

use std::io::net::ip::Ipv4Addr;
use iron::{status, Iron, Request, Response, IronResult};

use std::rc::Rc;
use time::Tm;
use std::collections::TreeMap;
use std::io::Timer;
use std::io::timer;
use std::time::duration::Duration;
use std::iter;
use sync::{Mutex, Arc};


/*
#[deriving(Clone)]
struct Node {

    right: Option<Box<Node>>,
    val: int


}

impl Node {

    fn new(x: int) -> Node {
        Node {right: None, val: x}
    }


    fn insert(&mut self, value: int) {
        let mut root = &mut *self;//.right.unwrap();//.clone();

        match root.right {
            Some(box ref mut s) => {
                println!("Some");
                root = s
            },
            None => println!("None")
        }
    }

}
*/

//extern crate iron;

//use std::io::net::ip::Ipv4Addr;
//use iron::{status, Iron, Request, Response, IronResult};


#[deriving(Show)]
struct User {

    id: uint,
    nickname: String,
    is_banned: bool

}

impl User {
    fn new(id: uint, nickname: String) -> User {
        User {id:id, nickname: nickname, is_banned: false}
    }
}


#[deriving(Show)]
struct Message {

    id: uint,
    user: User,
    time_create: Tm

}

impl Message {
    fn new(id: uint, user: User, time_create: Tm) -> Message {
        Message {id: id, user: user, time_create: time_create}
    }
}

#[deriving(Show)]
struct Group<'a> {

    name: String,
    id: uint,
    messages: TreeMap<uint, Message>
}

impl<'a> Group<'a> {
    fn new(name: String) -> Group<'a> {
        Group {name: name, id: 1, messages: TreeMap::new()}
    }

    fn get_name(&self) -> String {
        self.name.clone()
    }

    fn add_message(&mut self, message: Message) {

        //println!("mess id {}", message.id);
        self.messages.insert(message.id, message);
        //println!("add_message cnt {}", self.messages.len());
    }

    fn get_messages(&self) -> &TreeMap<uint, Message> {
        &self.messages
    }

}

struct Chat<'a> {

    groups: TreeMap<String, Group<'a>>,

}

impl<'a> Chat<'a> {

    fn new() -> Chat<'a> {
        return Chat { groups: TreeMap::new() }
    }

    /*fn create_group(& mut self, name: String) -> Group {
        let group = Group {name: name, id: 1};
        self.groups.push(group);
        return group
    }*/

    fn add_group(&mut self, group: Group<'a>) {
        self.groups.insert(group.get_name(), group);
    }

    fn get_group_by_name(&'a mut self, name: &str) -> Option<&mut Group<'a>> {
        let m = name.to_string();
        let b = self.groups.find_mut(&m);
        //b.as_mut()
        b

    }

}


fn main() {

    fn hello_world(_: &mut Request) -> IronResult<Response> {

        Ok(Response::with(status::Ok, "Hello, world"))
    }

    Iron::new(hello_world).listen(Ipv4Addr(127, 0, 0, 1), 3000);
    println!("On 3000");

    //let mut map = TreeMap::new();

    //map.insert(2i, "bar");
    //map.insert(1i, "foo");
    //map.insert(3i, "quux");

    // In ascending order by keys
    //for (key, value) in map.iter() {
      //  println!("{}: {}", key, value);
    //}

    let mut chat = Chat::new();

    let first_group = Group::new("first_group".to_string());

    chat.add_group(first_group);

    {
        match chat.get_group_by_name("first_group") {
            Some(group) => {
                println!("group exists {}", group);
                //let x: () = group;
            },
            None => println!("no such group")
        }
    }

    let interval = Duration::milliseconds(1000);
    let mut timer = Timer::new().unwrap();

    let metronome: Receiver<()> = timer.periodic(interval);

    println!("Countdown");
    for i in iter::range_step(1u, 3, 1) {
        // This loop will run once every second
        metronome.recv();

        let _t = "first_group";
        //let mut a = chat.get_group_by_name(_t);
        //let mut b = a.as_mut();
        //let mut mfirst_group = a.unwrap();

        let mut mfirst_group = chat.get_group_by_name(_t).unwrap();

        let user = User::new(i, format!("user{:u}", i));

        mfirst_group.add_message(Message::new(i, user, time::now()));

        println!("{}", i);
    }

    {
        for (k, v) in chat.get_group_by_name("first_group").unwrap().get_messages().iter() {
            println!("{} {}", k, v)
        }
    }


    let mut chat2 = Chat::new();
    let second_group = Group::new("second_group".to_string());

    chat2.add_group(second_group);

    let shared_chat = Arc::new(Mutex::new(chat2));

    for  i in iter::range_step(100u, 120, 1) {

        let local_chat_mutex = shared_chat.clone();

        spawn(proc() {
            let mut local_chat = local_chat_mutex.lock();
            println!("spawn {}", i);

            let user = User::new(i, format!("user{:u}", i));

            //let mut x: () = local_chat.get();

            match local_chat.get_group_by_name("second_group") {
                Some(_chat) => {
                    _chat.add_message(
                        Message::new(i, user, time::now())
                    );
                },
                None => println!("group second_group not found")
            }

        })

    }

    println!("before locked_chat");
    let mut locked_chat = shared_chat.lock();
    println!("after locked_chat");

    for (k, v) in locked_chat.get_group_by_name("second_group").unwrap().get_messages().iter() {
        println!("chat2 mess {}", k);
    }




}